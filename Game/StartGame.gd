extends Control

func _ready():
	for button in $UntukMargin/BagianTengah/pilihLevel/ButtonsAtas.get_children():
		button.connect("pressed",self,"_on_Button_pressed",[button.load_level])
	for button in $UntukMargin/BagianTengah/pilihLevel/ButtonsBawah.get_children():
		button.connect("pressed",self,"_on_Button_pressed",[button.load_level])
		
func _on_Button_pressed(load_level):
	get_tree().change_scene(load_level)