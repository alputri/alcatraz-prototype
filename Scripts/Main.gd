extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():

	var DialogNode = get_node("Dialog Box/DialogBox/RichTextLabel")
	var PlayerNode = get_node("Game/Player")
	
	DialogNode.dialog.append("I must get out of here! (PRESS SPACE)")
	DialogNode.dialog.append("Sneaking past these guards won't be easy...(PRESS SPACE)")
	DialogNode._initial_set()
