extends "res://Scripts/Enemy.gd"

var spritedir = "down"
var movedir = Vector2(0,1)

func spritedir_loop():
	match movedir:
		Vector2(-1,0):
			spritedir = "left"
		Vector2(1,0):
			spritedir = "right"
		Vector2(0,-1):
			spritedir = "up"
		Vector2(0,1):
			spritedir = "down"

func _ready():
	velocity.y = 1
	velocity = velocity.normalized() * speed
	set_physics_process(true)

func anim_switch(animation):
	var newanim = str(animation,spritedir)
	if $anim.current_animation != newanim:
		$anim.play(newanim)

func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		var motion = collision.remainder.bounce(collision.normal)
		velocity = velocity.bounce(collision.normal)
		move_and_collide(motion)

	if movedir != Vector2(0,0):
		anim_switch("walk")
	else:
		anim_switch("idle")