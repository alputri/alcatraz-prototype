extends KinematicBody2D

const TYPE = "PLAYER"
const DAMAGE = 2

export (int) var speed = 200
var movedir = Vector2(0,0)
var spritedir = "down"
var canMove = false
onready var initialPos = get_position()

func controls_loop():
	
	if canMove == true:
	
		var left = Input.is_action_pressed('ui_left')
		var right = Input.is_action_pressed('ui_right')
		var up = Input.is_action_pressed('ui_up')
		var down = Input.is_action_pressed('ui_down')
	
		movedir.x = -int(left) + int(right)
		movedir.y = -int(up) + int(down)

func movement_loop():
	var motion = movedir.normalized() * speed * 1.5
	move_and_slide(motion, Vector2(0,0))

func damage_loop():
	for body in $hitbox.get_overlapping_bodies():
		if body.get("DAMAGE") == 1 and body.get("TYPE") != TYPE:
			var ouch = AudioStreamPlayer.new()
			ouch.stream = load("res://Songs/zapsplat_cartoon_character_high_pitched_says_oops_23586.wav")
			self.add_child(ouch)
			ouch.play()
			kill()

func spritedir_loop():
	match movedir:
		Vector2(-1,0):
			spritedir = "left"
		Vector2(1,0):
			spritedir = "right"
		Vector2(0,-1):
			spritedir = "up"
		Vector2(0,1):
			spritedir = "down"

func anim_switch(animation):
	var newanim = str(animation,spritedir)
	if $anim.current_animation != newanim:
		$anim.play(newanim)

func _physics_process(delta):
	controls_loop()
	movement_loop()
	spritedir_loop()
	damage_loop()
	
	if movedir != Vector2(0,0):
		anim_switch("walk")
	else:
		anim_switch("idle")

func kill():
	set_position(initialPos)

func _on_DialogBox_hide():
	canMove = true
