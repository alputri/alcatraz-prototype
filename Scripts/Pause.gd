extends Control

func _ready():
	for button in $Menu.get_children():
		button.connect("pressed",self,"_on_Button_pressed",[button.load_level])
		
func _on_Button_pressed(load_level):
	get_tree().paused = false
	get_tree().change_scene(load_level)
	
func _input(event):
	if event.is_action_pressed("pause"):
		var new_pause_state = not get_tree().paused
		get_tree().paused = new_pause_state
		visible = new_pause_state